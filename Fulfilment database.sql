use master;

IF DB_ID (N'hotel') IS NOT NULL
begin;
drop database hotel;
end;

create database hotel;

use [hotel];
--GRANT CONTROL ON DATABASE::hotel TO [root];
--GRANT EXECUTE ON OBJECT::msdb.dbo.hotel TO [root]
--GRANT ALTER ANY USER TO [root];
--GRANT ALTER ON [hotel] TO [root];

-- SET IDENTITY_INSERT [dbo].[hotel] ON;

IF OBJECT_ID(N'dbo.Apartments', N'U') IS NOT NULL
DROP TABLE [Apartments];
IF NOT EXISTS (
    SELECT * FROM sys.tables t 
    JOIN sys.schemas s ON (t.schema_id = s.schema_id) 
    WHERE s.name = 'dbo' AND t.name = 'Apartments')	
		CREATE TABLE dbo.Apartments (
			id int IDENTITY(1,1) PRIMARY KEY,
			apartment_number int,
			status varchar(10),
			type varchar(10),
			number_of_rooms int,
			number_of_beds int,
			price float,
			photo varchar(100)
        );

MERGE dbo.Apartments WITH (SERIALIZABLE) AS T
USING (VALUES (1, 1, 'Free', 'Bad', 1, 1, 20, 'apartment_68.jpg')) AS U (id, apartment_number, status, type, number_of_rooms, number_of_beds, price, photo)
    ON U.id = T.id
WHEN MATCHED THEN 
    UPDATE SET T.apartment_number = U.apartment_number,
				T.status = U.status				
WHEN NOT MATCHED THEN
    INSERT (apartment_number, status, type, number_of_rooms, number_of_beds, price, photo) 
    VALUES (U.apartment_number, U.status, U.type, U.number_of_rooms, U.number_of_beds, U.price, U.photo);

MERGE dbo.Apartments WITH (SERIALIZABLE) AS T
USING (VALUES (2, 2, 'Free', 'Normal', 2, 2, 30, 'apartment_213.jpg')) AS U (id, apartment_number, status, type, number_of_rooms, number_of_beds, price, photo)
    ON U.id = T.id
WHEN MATCHED THEN 
    UPDATE SET T.apartment_number = U.apartment_number,
				T.status = U.status				
WHEN NOT MATCHED THEN
    INSERT (apartment_number, status, type, number_of_rooms, number_of_beds, price, photo) 
    VALUES (U.apartment_number, U.status, U.type, U.number_of_rooms, U.number_of_beds, U.price, U.photo);

MERGE dbo.Apartments WITH (SERIALIZABLE) AS T
USING (VALUES (3, 3, 'Free', 'Good', 3, 3, 50, 'apartment_707.jpg')) AS U (id, apartment_number, status, type, number_of_rooms, number_of_beds, price, photo)
    ON U.id = T.id
WHEN MATCHED THEN 
    UPDATE SET T.apartment_number = U.apartment_number,
				T.status = U.status				
WHEN NOT MATCHED THEN
    INSERT (apartment_number, status, type, number_of_rooms, number_of_beds, price, photo) 
    VALUES (U.apartment_number, U.status, U.type, U.number_of_rooms, U.number_of_beds, U.price, U.photo);

	
IF OBJECT_ID(N'dbo.Users', N'U') IS NOT NULL
DROP TABLE [dbo].[Users];
IF NOT EXISTS (
    SELECT * FROM sys.tables t 
    JOIN sys.schemas s ON (t.schema_id = s.schema_id) 
    WHERE s.name = 'dbo' AND t.name = 'Users') 	
    CREATE TABLE dbo.Users (
		id int IDENTITY(1,1) PRIMARY KEY,
        userInformation_id int,
        email varchar(50),
        password varchar(50),
        role_id int
        );
		
IF OBJECT_ID(N'dbo.Roles', N'U') IS NOT NULL
DROP TABLE [dbo].[Roles];
IF NOT EXISTS (
    SELECT * FROM sys.tables t 
    JOIN sys.schemas s ON (t.schema_id = s.schema_id) 
    WHERE s.name = 'dbo' AND t.name = 'Roles') 	
    CREATE TABLE dbo.Roles (
		id int IDENTITY(1,1) PRIMARY KEY,
        role varchar(50)
        );		

MERGE dbo.Roles WITH (SERIALIZABLE) AS T
USING (VALUES (1, 'User')) AS U (id, role)
    ON U.id = T.id
WHEN MATCHED THEN 
    UPDATE SET T.role = U.role			
WHEN NOT MATCHED THEN
    INSERT (role) 
    VALUES (U.role);

MERGE dbo.Roles WITH (SERIALIZABLE) AS T
USING (VALUES (2, 'Admin')) AS U (id, role)
    ON U.id = T.id
WHEN MATCHED THEN 
    UPDATE SET T.role = U.role			
WHEN NOT MATCHED THEN
    INSERT (role) 
    VALUES (U.role);

IF OBJECT_ID(N'dbo.UserOrders', N'U') IS NOT NULL
DROP TABLE [dbo].[UserOrders];
IF NOT EXISTS (
    SELECT * FROM sys.tables t 
    JOIN sys.schemas s ON (t.schema_id = s.schema_id) 
    WHERE s.name = 'dbo' AND t.name = 'UserOrders') 	
    CREATE TABLE dbo.UserOrders (
        id int IDENTITY(1,1) PRIMARY KEY,
        status varchar(10),
        start_time datetime,
        lease_duration int,
        user_id int,
        apartment_id int
        );
		
IF OBJECT_ID(N'dbo.UserInformation', N'U') IS NOT NULL
DROP TABLE [dbo].[UserInformation];
IF NOT EXISTS (
    SELECT * FROM sys.tables t 
    JOIN sys.schemas s ON (t.schema_id = s.schema_id) 
    WHERE s.name = 'dbo' AND t.name = 'UserInformation') 	
    CREATE TABLE dbo.UserInformation (
        id int IDENTITY(1,1) PRIMARY KEY,
        name varchar(20),
        surname varchar(20),
        phone varchar(30)
        );