<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<jsp:include page="fragments/header.jsp"/>
<head>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="localization.language" var="loc"/>
<fmt:setBundle basename="information" var="info"/>
<h1><%= "Hello World!" %>
</h1>
<br/>
<a href="controller">Controller</a>
<a href="hello-servlet">Hello Servlet</a>
<a href="/hotel?">/hotel?</a>
</body>
</html>