package com.entity;

public interface Identifiable {
    int getId();
}
