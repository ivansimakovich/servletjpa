package com.controller.command;

import com.controller.context.RequestContextHelper;

import jakarta.servlet.http.HttpServletResponse;

public interface Command {
    CommandResult execute(RequestContextHelper helper, jakarta.servlet.http.HttpServletResponse response);
}
