package com.controller.command.impl.transition;

import com.controller.command.Command;
import com.controller.command.CommandResult;
import com.controller.command.CommandResultType;
import com.controller.context.RequestContext;
import com.controller.context.RequestContextHelper;

import jakarta.servlet.http.HttpServletResponse;


public class GoToAddApartmentCommand implements Command {
    private static final String PAGE = "WEB-INF/view/addApartment.jsp";


    @Override
    public CommandResult execute(RequestContextHelper helper, HttpServletResponse response) {
        RequestContext requestContext = helper.createContext();


        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandResultType.FORWARD);
    }
}