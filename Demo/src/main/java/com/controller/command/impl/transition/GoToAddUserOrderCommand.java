package com.controller.command.impl.transition;

import com.controller.command.Command;
import com.controller.command.CommandResult;
import com.controller.command.CommandResultType;
import com.controller.context.RequestContext;
import com.controller.context.RequestContextHelper;


import jakarta.servlet.http.HttpServletResponse;

public class GoToAddUserOrderCommand implements Command {
    private static final String PAGE = "WEB-INF/view/addUserOrder.jsp";
    private static final String APARTMENT_ID="apartment_id";


    @Override
    public CommandResult execute(RequestContextHelper helper, HttpServletResponse response) {
        RequestContext requestContext = helper.createContext();
     /*   String apartmentId=requestContext.getRequestParameter(APARTMENT_ID);
        requestContext.addRequestAttribute(APARTMENT_ID,apartmentId);*/
        helper.updateRequest(requestContext);
        return new CommandResult(PAGE, CommandResultType.FORWARD);
    }
}
