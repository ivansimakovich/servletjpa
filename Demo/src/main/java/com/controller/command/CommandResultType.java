package com.controller.command;

public enum CommandResultType {
    FORWARD, REDIRECT
}
