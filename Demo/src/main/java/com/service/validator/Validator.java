package com.service.validator;

public interface Validator {
    boolean isValid(String expression);
}
