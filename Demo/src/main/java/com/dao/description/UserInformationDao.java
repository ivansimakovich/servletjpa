package com.dao.description;

import com.dao.Dao;
import com.entity.UserInformation;
import com.exeptions.DaoException;


public interface UserInformationDao extends Dao<UserInformation> {

    void updateById(int id, UserInformation userInformation) throws DaoException;
}
