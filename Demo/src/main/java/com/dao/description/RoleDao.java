package com.dao.description;

import com.dao.Dao;
import com.entity.Role;
import com.exeptions.DaoException;

import java.util.Optional;

public interface RoleDao extends Dao<Role> {


    Optional<Role> findByName(String name) throws DaoException;
}